<?php
/**
 * @file
 * channelaustin_reservation_example.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function channelaustin_reservation_example_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->api_version = 1;
  $field_group->identifier = 'group_extended_reservations|node|reservations_reservation|default';
  $field_group->group_name = 'group_extended_reservations';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'reservations_reservation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Extended Reservations',
    'weight' => '3',
    'children' => array(
      0 => 'field_extend_reservation_request',
      1 => 'field_extend_reservation_options',
      2 => 'field_extend_reservation_notes',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_extended_reservations|node|reservations_reservation|default'] = $field_group;

  return $export;
}
