<?php
/**
 * @file
 * channelaustin_reservation_example.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function channelaustin_reservation_example_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'merci_extended_res';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'MERCI: Extended Reservations';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'MERCI: Extended Reservations';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Extended Reservation Notes */
  $handler->display->display_options['fields']['field_extend_reservation_notes']['id'] = 'field_extend_reservation_notes';
  $handler->display->display_options['fields']['field_extend_reservation_notes']['table'] = 'field_data_field_extend_reservation_notes';
  $handler->display->display_options['fields']['field_extend_reservation_notes']['field'] = 'field_extend_reservation_notes';
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_notes']['field_api_classes'] = 0;
  /* Field: Content: Extended Reservation Options */
  $handler->display->display_options['fields']['field_extend_reservation_options']['id'] = 'field_extend_reservation_options';
  $handler->display->display_options['fields']['field_extend_reservation_options']['table'] = 'field_data_field_extend_reservation_options';
  $handler->display->display_options['fields']['field_extend_reservation_options']['field'] = 'field_extend_reservation_options';
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_options']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_options']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_extend_reservation_options']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_extend_reservation_options']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_extend_reservation_options']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'reservations_reservation' => 'reservations_reservation',
  );
  /* Filter criterion: Content: Extended Reservation Request (field_extend_reservation_request) */
  $handler->display->display_options['filters']['field_extend_reservation_request_value']['id'] = 'field_extend_reservation_request_value';
  $handler->display->display_options['filters']['field_extend_reservation_request_value']['table'] = 'field_data_field_extend_reservation_request';
  $handler->display->display_options['filters']['field_extend_reservation_request_value']['field'] = 'field_extend_reservation_request_value';
  $handler->display->display_options['filters']['field_extend_reservation_request_value']['value'] = array(
    1 => '1',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'admin/merci/extended-reservations';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['merci_extended_res'] = $view;

  return $export;
}
