<?php
/**
 * @file
 * channelaustin_reservation_example.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function channelaustin_reservation_example_field_default_fields() {
  $fields = array();

  // Exported field: 'node-reservations_reservation-field_extend_reservation_notes'
  $fields['node-reservations_reservation-field_extend_reservation_notes'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_extend_reservation_notes',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(
        'field_permissions' => array(
          'create' => 'create',
          'edit' => 'edit',
          'edit own' => 0,
          'view' => 'view',
          'view own' => 0,
        ),
      ),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'reservations_reservation',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '4',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_extend_reservation_notes',
      'label' => 'Extended Reservation Notes',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '8',
      ),
    ),
  );

  // Exported field: 'node-reservations_reservation-field_extend_reservation_options'
  $fields['node-reservations_reservation-field_extend_reservation_options'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '4',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_extend_reservation_options',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Out of Area' => 'Take Resources outside of Bastrop, Caldwell, Hays, Travis, or Williamson counties (10% deposit required)',
          'more_days' => 'Make a Reservation for Resources more than 30 days in advance (but less than 60)',
          'More than time allowed' => 'Make a Reservation for more than time allowed for that Resource',
          'Combined Field and Studio' => 'Reserve Field and Studio Resources at the same time',
        ),
        'allowed_values_function' => '',
        'field_permissions' => array(
          'create' => 'create',
          'edit' => 'edit',
          'edit own' => 0,
          'view' => 'view',
          'view own' => 0,
        ),
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'reservations_reservation',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '<strong>Please note: To make a Reservation for more than the time allowed for that Resource, select your Check out Date & Time and Check in Date & Time as you normally would, then check the appropriate box above, and write a note to your Staff Guide for when you would like to set the Check in Date & Time.</strong>',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_extend_reservation_options',
      'label' => 'Extended Reservation Options',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '7',
      ),
    ),
  );

  // Exported field: 'node-reservations_reservation-field_extend_reservation_request'
  $fields['node-reservations_reservation-field_extend_reservation_request'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_extend_reservation_request',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          0 => 'I do not want to extend my reservation',
          1 => 'I want to request an Extended Reservation',
        ),
        'allowed_values_function' => '',
        'field_permissions' => array(
          'create' => 'create',
          'edit' => 'edit',
          'edit own' => 0,
          'view' => 'view',
          'view own' => 0,
        ),
      ),
      'translatable' => '0',
      'type' => 'list_boolean',
    ),
    'field_instance' => array(
      'bundle' => 'reservations_reservation',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '2',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_extend_reservation_request',
      'label' => 'Extended Reservation Request',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 0,
        ),
        'type' => 'options_onoff',
        'weight' => '6',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('<strong>Please note: To make a Reservation for more than the time allowed for that Resource, select your Check out Date & Time and Check in Date & Time as you normally would, then check the appropriate box above, and write a note to your Staff Guide for when you would like to set the Check in Date & Time.</strong>');
  t('Extended Reservation Notes');
  t('Extended Reservation Options');
  t('Extended Reservation Request');

  return $fields;
}
